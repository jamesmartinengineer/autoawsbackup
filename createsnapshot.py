# AWS EC2/EBS backup script
# -------------------------
# Uplifted by James Martin (james.martin@engineer.com, james.martin@envisian.com.au - 0457 046 174)
#
# Original code source: https://giancarlopetrini.com/automate-snapshots/
#
# New source: https://bitbucket.org/jamesmartinengineer/autoawsbackup/
#
# Version 1.1 - 20180906: Added application aware backup process for running Windows EC2 instances. Improved Snapshot readability, disabled weekly backups
# Version 1.2 - 20190101: Made exception more verbose. TO DO: Add a better exception handler to capture different failure modes. If SSM is not working on a Windows server presently the script gets stuck in a loop and times out
# Version 1.3 - 20190114: Added devices to non-app aware backup tags. This makes restoring instances more straight forward.
# Version 1.4 - 20190711: So many bugs!? Fixed a cold linux backup problem and a string that was supposed to be a variable
# Version 1.5 - 20190723: Now has a terraform deploy script. EC2 role is still a bit funny

import traceback
import boto3  
import collections  
import datetime
import time

#Please mention your region name
#below line code is call cross region
ec = boto3.client('ec2', region_name='ap-southeast-2')
ssm = boto3.client('ssm', region_name='ap-southeast-2')
    
    #begins lambda function
def lambda_handler(event, context):  

        reservations = ec.describe_instances(
            Filters=[
                {'Name': 'tag-key', 'Values': ['Backup', 'True']},
            ]
        ).get(
            'Reservations', []
        )
    
        instances = sum(
            [
                [i for i in r['Instances']]
                for r in reservations
            ], [])
    
        print("Number of the Instances : %d" % len(instances))
    
        to_tag = collections.defaultdict(list)
        print(instances)
        for instance in instances:
            try:
                retention_days = [
                    int(t.get('Value')) for t in instance['Tags']
                    if t['Key'] == 'Retention'][0]
            except IndexError:
                # Please give your retention period day
                retention_days = 7
            
            current_state = instance['State']['Name']
            
            try:
                operating_system = instance['Platform']
            except KeyError:
                operating_system = "null"
            if(operating_system=="windows" and current_state=="running"):
                # if machine is on and Windows (CreateVSSSnapshot via Run Command)
                
                #To get all the instance tags details
                for tag in instance['Tags']:
                    # To store the instance tag value
                    Instancename= tag['Value']
                    # To store the instance key value
                    key= tag['Key']
                    # Below the code is create Snapshot name as instance Name 
                    if key == 'Name' :
                        ins_name = Instancename
                        
                        print("About to try for " +Instancename)
                        print("Found hot instance %s" % (instance['InstanceId']))
                        InstanceId = instance['InstanceId']
                        backupdesc = "Hot AppAware Backup of " + ins_name
                        version = 'Daily'
                        # Here to get current date
                        timenow = datetime.datetime.today()
                        
                        snapshot = ins_name + timenow.strftime(" %y-%m-%d %H:00")
                        
                        delete_date = datetime.date.today() + datetime.timedelta(days=retention_days)
                        # to mention the current date formet
                        delete_fmt = delete_date.strftime('%Y-%m-%d')
                        
                        # Create some tags to identify the backup and retention
                        backuptags = "Key=Name,Value=" + snapshot + ";Key=DeleteOn,Value=" + delete_fmt + ";Key=Version,Value=" + version
                        
                        # if machine is off or Linux (Create EC2 level snapshot) this this code would execute:
                        while True:
                            attempt = 0
                            print("instance ID is " + InstanceId) # debug
                            try:
                                
                                #ssm.send_command(InstanceIds=[instance['InstanceId']], DocumentName='AWSEC2-CreateVssSnapshot', Comment='AppAware Backup', Parameters={'description':[backupdesc],'tags':[backuptags]})
                                ssm.send_command(InstanceIds=[InstanceId], DocumentName='AWSEC2-CreateVssSnapshot', Comment='AppAware Backup', Parameters={'description':[backupdesc],'tags':[backuptags]})
                                print("snap started for %s" %ins_name)
                            except :
                                print("API appears busy. Starting exponential back-off and retry- For HOT backup")
                                print(ins_name)
                                traceback.print_exc()
                                time.sleep(1+attempt)
                                attempt = attempt + 1
                                continue
                            break
            else:
                for dev in instance['BlockDeviceMappings']:
                    if dev.get('Ebs', None) is None:
                        continue
                    vol_id = dev['Ebs']['VolumeId']
                    ThisDevice = dev['DeviceName']
                    #To get all the instance tags details
                    for tag in instance['Tags']:
                        # To store the instance tag value
                        Instancename= tag['Value']
                        # To store the instance key value
                        key= tag['Key']
                        # Below the code is create Snapshot name as instance Name 
                        if key == 'Name' :
                            ins_name = Instancename
                            print("Found EBS volume %s on instance %s" % (
                                vol_id, instance['InstanceId']))
                            if(current_state == "running"):
                                cur_state = "Hot "
                            else:
                                cur_state = "Cold "                            
                            description1 = cur_state + " Storage Backup of " + Instancename
                            # if machine is off or Linux (Create EC2 level snapshot) this this code would execute:
                            while True:
                                attempt = 0
                                try:
                                    snap = ec.create_snapshot(
                                        VolumeId=vol_id,
                                        Description=description1,
                                        TagSpecifications=[
                                            {
                                                'ResourceType': 'snapshot',
                                                'Tags': [
                                                    {
                                                        'Key': 'Device',
                                                        'Value': ThisDevice
                                                    },
                                                ]
                                            },
                                        ]
                                        )
                                    print("snap %s" %snap)
                                except:
                                    print("API appears busy. Starting exponential back-off and retry- For COLD backup")
                                    time.sleep(1+attempt)
                                    attempt = attempt + 1
                                    continue
                                break
            
                    to_tag[retention_days].append(snap['SnapshotId'])
                    
                    print("Retaining snapshot %s of volume %s from instance %s for %d days. Device %s" % (
                        snap['SnapshotId'],
                        vol_id,
                        instance['InstanceId'],
                        retention_days,
                        ThisDevice
                    ))
                    for retention_days in to_tag.keys():
                        #uncomment the following for the tool to create weekly backups on Mondays
                        
                        # if this runs on a Monday, then start doing weekly, monthly, or whatever....
                        #if datetime.datetime.today().weekday() == 0:
                            # set retention to 4 weeks, and tag as Version: Weekly
                        #   delete_date = datetime.date.today() + datetime.timedelta(days=30)
                        #   print "Snapshot being marked as weekly. Will keep until %s" % str(delete_date)
                        #   version = 'Weekly'
                        #else:
                        #   version = 'Daily'
                        #   delete_date = datetime.date.today() + datetime.timedelta(days=retention_days)
                        
                        version = 'Daily'
                        delete_date = datetime.date.today() + datetime.timedelta(days=retention_days)	
                        
                        snap = ins_name + str('_')
                        # Here to get current date
                        timenow = datetime.datetime.today()
                        
                        snapshot = snap + timenow.strftime(" %y-%m-%d %H:00")
                        # to mention the current date formet
                        delete_fmt = delete_date.strftime('%Y-%m-%d')
                        print("Will delete %d snapshots on %s" % (len(to_tag[retention_days]), delete_fmt))
                        # below code is create the name and current date as instance name
                        
                        while True:
                                attempt = 0
                                try:
                                    ec.create_tags(
                                    # this grabs the snapshot id from the list and applies the following tags
                                    Resources=to_tag[retention_days],
                                    Tags=[
                                    {'Key': 'DeleteOn', 'Value': delete_fmt},
                                    {'Key': 'Name', 'Value': snapshot },
                                    {'Key': 'Version', 'Value': version}
                                    ]
                                    )
                                except e:
                                    print("API appears busy. Starting exponential back-off and retry")
                                    time.sleep(1+attempt)
                                    attempt = attempt + 1
                                    continue
                                break
                        
                to_tag.clear()
