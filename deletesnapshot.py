# AWS EC2/EBS EBS snapshot cleanup script
# -------------------------
# Uplifted by James Martin (james.martin@engineer.com, james.martin@envisian.com.au - 0457 046 174)
#
# Original code source: https://giancarlopetrini.com/automate-snapshots/
#
# Version 1.1 - 20180910: Added automatic back-off/retry function for safe AWS API integration. Made compatible with Python 3.x

import boto3  
import datetime
import time

#[start configuration]
ec = boto3.client('ec2', region_name='ap-southeast-2')  
#[end configuration]

#[begin lambda function]
def lambda_handler(event, context):  
    # loop through each of the versions and run deletions
    # matching delete day + version tag
    versions = ['Daily', 'Weekly', 'Monthly', 'Quarterly']
    for version in versions:
        delete_on = datetime.date.today().strftime('%Y-%m-%d')
        filters = [
            {'Name': 'tag-key', 'Values': ['DeleteOn']},
            {'Name': 'tag-value', 'Values': [delete_on]},
            {'Name': 'tag-key', 'Values': ['Version']},
            {'Name': 'tag-value', 'Values': [version]}
        ]
        snapshot_response = ec.describe_snapshots(Filters=filters)
        for snap in snapshot_response['Snapshots']:
            print("Deleting snapshot %s" % snap['SnapshotId'])
            while True:
                attempt = 0
                try:
                    ec.delete_snapshot(SnapshotId=snap['SnapshotId'])
                except:
                    print("API appears busy. Starting exponential back-off and retry")
                    time.sleep(1+attempt)
                    attempt = attempt + 1
                    continue
                break
#[end lambda function]