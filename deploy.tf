########## IAM Role  ##########
resource "aws_iam_role" "AppAware-EC2-Backup-Role" {
  name = "AppAware-EC2-Backup-Role"

  assume_role_policy = <<-EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
            "ec2.amazonaws.com",
            "lambda.amazonaws.com"
            ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource "aws_iam_role" "AppAware-EC2-Backup-Role-EC2" {
  name = "AppAware-EC2-Backup-Role-EC2"

  assume_role_policy = <<-EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}



########## IAM Role Policy ##########
resource "aws_iam_role_policy" "AppAware-EC2-Backup-Policy" {
    name = "AppAware-EC2-Backup-Policy"
    role = "${aws_iam_role.AppAware-EC2-Backup-Role.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:CreateSnapshot",
                "ec2:DeleteSnapshot",
                "ec2:CreateTags",
                "ec2:ModifySnapshotAttribute",
                "ec2:ResetSnapshotAttribute"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:DescribeAssociation",
                "ssm:GetDeployablePatchSnapshotForInstance",
                "ssm:GetDocument",
                "ssm:GetManifest",
                "ssm:*",
                "ssm:GetParameters",
                "ssm:ListAssociations",
                "ssm:ListInstanceAssociations",
                "ssm:PutInventory",
                "ssm:PutComplianceItems",
                "ssm:PutConfigurePackageResult",
                "ssm:UpdateAssociationStatus",
                "ssm:UpdateInstanceAssociationStatus",
                "ssm:UpdateInstanceInformation"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssmmessages:CreateControlChannel",
                "ssmmessages:CreateDataChannel",
                "ssmmessages:OpenControlChannel",
                "ssmmessages:OpenDataChannel"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2messages:AcknowledgeMessage",
                "ec2messages:DeleteMessage",
                "ec2messages:FailMessage",
                "ec2messages:GetEndpoint",
                "ec2messages:GetMessages",
                "ec2messages:SendReply"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstanceStatus"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ds:CreateComputer",
                "ds:DescribeDirectories"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetEncryptionConfiguration",
                "s3:AbortMultipartUpload",
                "s3:ListMultipartUploadParts",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_lambda_function" "AppAware-EC2-Backup" {
  filename      = "data.zip"
  function_name = "AppAware-EC2-Backup"
  role          = "${aws_iam_role.AppAware-EC2-Backup-Role.arn}"
  handler       = "lambda_function.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  #source_code_hash = "${filebase64sha256("lambda_function_payload.zip")}"

  runtime = "python3.7"

  #environment {
  #  variables = {
  #    foo = "bar"
  #  }
  #}
}

resource "aws_cloudwatch_event_rule" "AppAware-EC2-Backup-Retention-Rule" {
    name = "AppAware-EC2-Retention-Daily-Event"
    description = "Triggers backup daily at 8pm"
    schedule_expression = "cron(0 20 * * ? *)"
}

resource "aws_cloudwatch_event_target" "AppAware-EC2-Backup-Retention-Target" {
  target_id = "AppAware-EC2-Backup-Retention-Target"
  rule      = "${aws_cloudwatch_event_rule.AppAware-EC2-Backup-Retention-Rule.name}"
  arn       = "${aws_lambda_function.AppAware-EC2-Snapshot-Retention.arn}"

##  run_command_targets {
#    key    = "tag:Name"
#    values = ["FooBar"]
#  }
#
#  run_command_targets {
#    key    = "InstanceIds"
#    values = ["i-162058cd308bffec2"]
#  }
}

resource "aws_cloudwatch_event_rule" "AppAware-EC2-Backup-Rule" {
    name = "AppAware-EC2-Backup-Daily-Event"
    description = "Triggers backup daily at 6pm"
    schedule_expression = "cron(0 18 * * ? *)"
}

resource "aws_cloudwatch_event_target" "AppAware-EC2-Backup-Target" {
  target_id = "AppAware-EC2-Backup-Target"
  rule      = "${aws_cloudwatch_event_rule.AppAware-EC2-Backup-Rule.name}"
  arn       = "${aws_lambda_function.AppAware-EC2-Backup.arn}"
}

resource "aws_lambda_function" "AppAware-EC2-Snapshot-Retention" {
  filename      = "retention.zip"
  function_name = "AppAware-EC2-Snapshot-Retention"
  role          = "${aws_iam_role.AppAware-EC2-Backup-Role.arn}"
  handler       = "lambda_function.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  #source_code_hash = "${filebase64sha256("lambda_function_payload.zip")}"

  runtime = "python3.7"

  #environment {
  #  variables = {
  #    foo = "bar"
  #  }
  #}
}