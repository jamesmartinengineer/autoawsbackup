
#pulls new version of repo, hard resets so that build.sh can be overitten and reset to 755
git reset --hard
git pull
chmod +755 build.sh

# zip some python files
mv createsnapshot.py lambda_function.py
zip data.zip lambda_function.py
mv deletesnapshot.py lambda_function.py
zip retention.zip lambda_function.py

#redeploy immediately
terraform apply -auto-approve